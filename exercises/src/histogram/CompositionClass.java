package histogram;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class CompositionClass {
    public static void main(String[] args) {

    }
}

class HistogramComp implements Iterable<String> {

    private Map<String, Integer> map;

    HistogramComp() {
        map = new HashMap<>();
    }

    public int getSize () {
        return map.size();
    }
//
//    public String getKey (String string) {
//        return
//    }

    public void insert (String string) {

        for (String s : string.split(" ")) {
            if (map.containsKey(s)) {
                map.put(s, map.get(s) + 1);
            }

            map.put(s, 1);
        }
    }

    @Override
    public Iterator<String> iterator() {
        return map.keySet().iterator();
    }
}
