package ArabianNights;

public class MagicLamp {
    private int wishes;
    private int genieCapacity;
    private int recharged;

    public MagicLamp(int genieCapacity) {
        this.genieCapacity = genieCapacity;
        this.wishes = 3;
    }

    public int getGenieCapacity() {
        return genieCapacity;
    }

    public int getRecharged () {
        return recharged;
    }

    public void recycleGenie(Genie genie) {

        if (recharged == 1) {
            System.out.println("you cannot recharge this lamp anymore!");
            return;
        }

        if (genie instanceof RecyclableDemon && !((RecyclableDemon) genie).recycled) {
            System.out.println("this lamp was recharged");
            genieCapacity = wishes;
            recharged++;
            ((RecyclableDemon) genie).recycled = true;
            return;
        }

        System.out.println("this is not a recyclable demon!!");

    }

    public Genie rub() {
        Genie genie = null;

        if (genieCapacity % 2 == 0 && genieCapacity > 0) {
            genie = new FriendlyGenie();
        } else if (genieCapacity % 2 != 0 && genieCapacity > 0) {
            genie = new GrumpyGenie();
        } else if (genieCapacity == 0) {
            genie = new RecyclableDemon();
        }

        genieCapacity--;
        return genie;
    }
}
