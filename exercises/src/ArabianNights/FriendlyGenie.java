package ArabianNights;

public class FriendlyGenie extends Genie {

    public FriendlyGenie () {
        super();
    }

    @Override
    public void grantWish() {

        System.out.println("i'm the friendly genie.");

        if (super.getWishes() == 0) {
            System.out.println("I cannot grant any more wishes.");
        }

        while (super.getWishes() > 0) {
            System.out.println("wish granted");
            super.wishGranted();
        }

    }
}
