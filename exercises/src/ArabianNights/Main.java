package ArabianNights;

public class Main {
    public static void main(String[] args) {

        MagicLamp lamp = new MagicLamp(2);

        Genie g1 = lamp.rub();
        g1.grantWish();
        g1.grantWish();
        g1.grantWish();
        g1.grantWish();

        Genie g2 = lamp.rub();
        g2.grantWish();
        g2.grantWish();
        g2.grantWish();
        g2.grantWish();

        Genie g3 = lamp.rub();
        g3.grantWish();

        lamp.recycleGenie(g2);

    }
}
