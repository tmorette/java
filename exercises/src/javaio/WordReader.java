package javaio;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

public class WordReader {
    public static void main(String[] args) {
        ListWords words = new ListWords();

        for (String s : words.read("/Users/codecadet/Documents/Projects/resources/file.txt")) {
            System.out.println(s);
        }
    }
}

class ListWords implements Iterable {

    private LinkedList<String> linkedList;

    ListWords() {
        linkedList = new LinkedList<>();
    }

    LinkedList<String> read(String file) {

        try (
                FileReader fileReader = new FileReader(file);
                BufferedReader bufferedReader = new BufferedReader(fileReader)
        ){

            String line;

            while ((line = bufferedReader.readLine()) != null) {
                Collections.addAll(linkedList, line.split(" "));
            }
        } catch (IOException e) {
            e.getMessage();
        }

        return linkedList;
    }

    @Override
    public Iterator iterator() {
        return linkedList.iterator();
    }
}
