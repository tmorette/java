package Exceptions;


import Exceptions.exceptions.FileNotFoundException;
import Exceptions.exceptions.NotEnoughPermissionsException;
import Exceptions.exceptions.NotEnoughSpaceException;
import Exceptions.filemanager.FileManager;

public class Main {

    public static void main(String[] args) {
        FileManager fileManager = new FileManager();

        try {
            fileManager.login();
            fileManager.createFile("test");
            fileManager.getFiles("test 2");
            fileManager.createFile("test 2");
            fileManager.createFile("test 3");
            fileManager.print();
        } catch (NotEnoughSpaceException e) {
             fileManager.increaseSize();
        } catch (NotEnoughPermissionsException e) {
            System.out.println("you're not allowed to make this operation");
        } catch (FileNotFoundException e) {
            System.out.println("file not found");
        }

        fileManager.print();
    }
}
