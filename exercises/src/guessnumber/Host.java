package guessnumber;

public class Host {
    private static int getRandomNumber(int limit) {
        return Random.generateRandomNumber(limit);
    }

    private static Player playingGame(int limit, Player player1, Player player2) {
        Player[] players = new Player[2];
        players[0] = player1;
        players[1] = player2;

        int number = getRandomNumber(limit);

        for (Player player : players) {
            if (player.getRandomNumber(limit) == number) {
                return player;
            }
        }

        return null;
    }

    private static void checkWinner(Player player) {

        if (player == null) {
            System.out.println("Nobody guesses it right.");
        } else {
            System.out.println("Winner is: " + player.getName());
        }
    }

    public void startGame(int limit, int rounds, Player player1, Player player2) {
        Player player = null;

        while (rounds > 0 && player == null) {
            player = playingGame(limit, player1, player2);
            rounds--;
        }

        checkWinner(player);

    }
}
