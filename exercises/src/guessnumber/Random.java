package guessnumber;

public class Random {
    public static int generateRandomNumber (int limit) {
        return (int) Math.ceil(Math.random() * limit);
    }
}
