package SniperElite.Objects;

public interface Destroyable {
    void hit (int damage);

    boolean isDestroyed();

    int getLife();
}
