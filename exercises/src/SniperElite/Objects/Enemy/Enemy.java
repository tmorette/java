package SniperElite.Objects.Enemy;

import SniperElite.Objects.Destroyable;
import SniperElite.Objects.GameObject;

public abstract class Enemy extends GameObject implements Destroyable {
    private int health;
    private boolean destroyed;

    public Enemy () {
        this.health = 100;
    }

    public boolean isDestroyed() {
        return destroyed;
    }

    public int getLife() {
        return health;
    }

    public void destroy() {
        destroyed = true;
    }

    @Override
    public void hit (int damage) {

        health -= damage;

        if (health <= 0) {
            health = 0;
            destroy();
        }

        System.out.println(getMessage());
    }

    public String getMessage() {
        if (health == 0) {
            return "---enemy is destroyed---\n";
        }

        return "---enemy have " + getLife() + " of health!---\n";
    }
}
