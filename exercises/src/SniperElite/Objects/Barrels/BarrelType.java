package SniperElite.Objects.Barrels;

public enum BarrelType {
    PLASTIC(20),
    WOOD(50),
    METAL(80);

    private int maxDamage;

    BarrelType(int maxDamage) {
        this.maxDamage = maxDamage;
    }

    public int getMaxDamage() {
        return maxDamage;
    }

    public static BarrelType getBarrelType () {
        return BarrelType.values()[(int) (Math.random() * BarrelType.values().length)];
    }
}
