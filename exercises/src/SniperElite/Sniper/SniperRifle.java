package SniperElite.Sniper;


import SniperElite.Objects.Barrels.Barrel;
import SniperElite.Objects.Destroyable;
import SniperElite.Objects.Enemy.BodyParts;
import SniperElite.Objects.Enemy.Enemy;

public class SniperRifle {

    private void bodyPartHit(Destroyable target, int damage) {
        BodyParts bodyPart = BodyParts.getBodyPart();
        int criticalHit = 40;

        if (damage >= criticalHit) {
            System.out.println("Sniper: wow... that's a critical!!");
        }

        switch (bodyPart) {
            case HEAD:
                System.out.println("Sniper: hot damn!! i just see a brain flying?!");
                damage = 100;
                break;
            case ARMS:
                System.out.println("Sniper: i think i just wound his arm...");
                break;
            case LEGS:
                System.out.println("Sniper: say goodbye to your knee!!");
                break;
            default:
                System.out.println("Sniper: hit him...");
                break;
        }

        System.out.println("---sniper hit the enemy for " + damage + " damage---");
        target.hit(damage);
    }

    private void shot(Destroyable target) {
        int damage = (int) ((Math.random() * 35) + 15);

        if (Math.random() < 0.3 && !(target instanceof Barrel)) {
            System.out.println("Sniper: crap... missed the shot!");
            return;
        }

        if (target instanceof Enemy) {
            bodyPartHit(target, damage);
            return;
        }

        System.out.println("---sniper hit the enemy for " + damage + " damage---");
        target.hit(damage);
    }

    public void shotEnemy(Destroyable target) {
        int shotsFired = 0;

        System.out.println("Sniper: " + target.getClass().getSimpleName() + " spotted!!");
        System.out.println("---enemy has " + target.getLife() + "---");

        while (!target.isDestroyed()) {
            if (shotsFired != 0) {
                System.out.println("shooting again...\n");
            }

            shot(target);
            shotsFired++;
        }

        System.out.println("Sniper: Target is ANNIHILATED!!\nthis one takes me "
                + shotsFired + " shots.\nNEXT!!");
    }
}
