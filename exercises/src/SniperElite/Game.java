package SniperElite;

import SniperElite.Objects.Barrels.Barrel;
import SniperElite.Objects.Destroyable;
import SniperElite.Objects.Enemy.ArmouredEnemy;
import SniperElite.Objects.Enemy.SoldierEnemy;
import SniperElite.Objects.GameObject;
import SniperElite.Objects.Tree.Tree;
import SniperElite.Sniper.SniperRifle;


public class Game {

    private GameObject[] gameObject;

    private void createObjects(int numberOfObjects) {

        gameObject = new GameObject[numberOfObjects];

        for (int i = 0; i < numberOfObjects; i++) {

            double random = Math.random();

            if (random < 0.1) {
                gameObject[i] = new Tree();
                continue;
            }

            if (random < 0.25) {
                gameObject[i] = new Barrel();
                continue;
            }

            if (random < 0.5) {
                gameObject[i] = new ArmouredEnemy();
                continue;
            }

            gameObject[i] = new SoldierEnemy();
        }
    }

    public void start() {
        createObjects(10);
        SniperRifle sniper = new SniperRifle();

        for (GameObject g : gameObject) {
            if (!(g instanceof Destroyable)) {
                System.out.println(g.getMessage());
                continue;
            }

            sniper.shotEnemy((Destroyable) g);
            System.out.println();

        }
    }
}
