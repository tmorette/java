package rockpaperscissors;

public class Game {

    private Player finalResult(Player playerOne, Player playerTwo) {
        if (playerOne.getVictories() > playerTwo.getVictories()) {
            return playerOne;
        } else if (playerOne.getVictories() < playerTwo.getVictories()) {
            return playerTwo;
        }

        return null;
    }

    private Player checkHandResult(Player playerOne, Player playerTwo) {
        Player winner = playerOne;
        HandTypes playerOneHand = playerOne.randomResult();
        HandTypes playerTwoHand = playerTwo.randomResult();

        if (playerOneHand == playerTwoHand) {
            return winner;
        }

        switch (playerOneHand) {
            case ROCK:
                if (playerTwoHand == HandTypes.PAPER) {
                    winner = playerTwo;
                }
                break;
            case PAPER:
                if (playerTwoHand == HandTypes.SCISSORS) {
                    winner = playerTwo;
                }
                break;
            case SCISSORS:
                if (playerTwoHand == HandTypes.ROCK) {
                    winner = playerTwo;
                }
                break;
        }

        return winner;
    }

    private void roundWinner(Player result, Player playerOne, Player playerTwo) {
        if (result.equals(playerOne)) {
            playerOne.win();
        } else if (result.equals(playerTwo)) {
            playerTwo.win();
        }
    }

    public void gameStart(int rounds, Player playerOne, Player playerTwo) {

        while (rounds >= 0) {
            Player result = checkHandResult(playerOne, playerTwo);

            if (result != null) {
                roundWinner(result, playerOne, playerTwo);
                System.out.println("round: " + rounds + " player one: " + playerOne.getVictories() + " player two: " + playerTwo.getVictories());
                rounds--;
            }
        }

        if (finalResult(playerOne, playerTwo) == playerOne) {
            System.out.println("\n" + playerOne.getName() + " won!! Congratulations " + playerOne.getName());
        } else if (finalResult(playerOne, playerTwo) == playerTwo) {
            System.out.println("\n" + playerTwo.getName() + " won!! Congratulations " + playerTwo.getName());
        } else {
            System.out.println("\nOh crap, it's a Draw!!!");
        }

    }
}
