package bank;

public class BankMain {
    public static void main(String[] args) {
        Client client = new Client("thiago");

        System.out.println(client.getName());
        client.getMoney();

        client.deposit(1400);
        client.getMoney();

        client.withdraw(100);
        client.getMoney();

        client.deposit(1100);
        client.getMoney();
    }
}
