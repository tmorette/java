package bank;

public class Client {
    private String name;
    private Wallet wallet;
    private BankAccount bankAccount;

    public Client(String name) {
        this.name = name;
        this.wallet = new Wallet(1400);
        this.bankAccount = new BankAccount();
    }

    public String getName() {
        return name;
    }

    public void getMoney() {
        double total = wallet.getAmmountMoney() + bankAccount.getAmmountMoney();
        System.out.println("---------------");
        System.out.println("bank: " + bankAccount.getAmmountMoney());
        System.out.println("Wallet: " + wallet.getAmmountMoney());
        System.out.println("Total: " + total);
        System.out.println("---------------");

    }

    public void deposit(double value) {
        if (wallet.checkLimit() > value) {
            wallet.deposit(value);
            System.out.println(value + " deposited in wallet");
            return;
        }

        bankAccount.deposit(value);
        System.out.println(value + " deposited in bank");
    }

    public void withdraw (double value) {
        if (wallet.getAmmountMoney() > value) {
            wallet.withdraw(value);
            System.out.println(value + " withdrawed from wallet");
            return;
        } else if (bankAccount.getAmmountMoney() > value){
            bankAccount.withdraw(value);
            System.out.println(value + " withdrawed from bank");
            return;
        }

        System.err.println("you try to withdraw " + value + " but yout don't have that ammount.");
    }
}
