package bank;

public class BankAccount {
    private double ammountMoney;

    public double getAmmountMoney() {
        return ammountMoney;
    }

    public void withdraw (double ammount) {
        if (this.ammountMoney > ammount) {
            ammountMoney -= ammount;
        }
    }

    public void deposit (double ammount) {
        this.ammountMoney += ammount;
    }
}
