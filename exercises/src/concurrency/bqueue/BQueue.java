package concurrency.bqueue;


import java.util.concurrent.ArrayBlockingQueue;

/**
 * Blocking Queue
 * @param <T> the type of elements stored by this queue
 */
public class BQueue<T> {

   private ArrayBlockingQueue<T> queue;
   private int limit;

    /**
     * Constructs a new queue with a maximum size
     * @param limit the queue size
     */
    public BQueue(int limit) {
        this.limit = limit;
        queue = new ArrayBlockingQueue<>(limit, true);

    }

    /**
     * Inserts the specified element into the queue
     * Blocking operation if the queue is full
     * @param data the data to add to the queue
     */
    public synchronized void offer(T data) {

        while (limit <= getSize()) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println("BQueue#offer(T)");
                System.out.println(e.getMessage());
            }
        }

        notifyAll();
        queue.add(data);
    }

    /**
     * Retrieves and removes data from the head of the queue
     * Blocking operation if the queue is empty
     * @return the data from the head of the queue
     */
    public synchronized T poll() {

        while (getSize() <= 0) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println("BQueue#poll()");
                System.out.println(e.getMessage());
            }
        }

        notifyAll();
        return queue.poll();
    }

    /**
     * Gets the number of elements in the queue
     * @return the number of elements
     */
    private int getSize() {
        return queue.size();
    }

    /**
     * Gets the maximum number of elements that can be present in the queue
     * @return the maximum number of elements
     */
    private int getLimit() {
        return limit;
    }

}
