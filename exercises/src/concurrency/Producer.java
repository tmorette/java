package concurrency;

import concurrency.bqueue.BQueue;

/**
 * Produces and stores integers into a blocking queue
 */
public class Producer implements Runnable {

    private final BQueue<Integer> queue;
    private int elementNum;

    /**
     * @param queue      the blocking queue to add elements to
     * @param elementNum the number of elements to produce
     */
    Producer(BQueue queue, int elementNum) {
        this.queue = queue;
        this.elementNum = elementNum;
    }

    @Override
    public void run() {
        int product = 1;

        for (int i = 1; i <= elementNum; i++) {
            queue.offer(product);
            System.out.println(Thread.currentThread().getName() + " added ->  " + product);

            product++;
        }

    }

}
