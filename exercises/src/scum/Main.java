package scum;

public class Main {
    public static void main(String[] args) {
        Machine<Integer> machine = new Machine<>();
        System.out.println(machine.monoOperation(2, var -> var + 2));
        System.out.println(machine.monoOperation(4, var -> var + 2));

        System.out.println(machine.biOperation(3, 4, (var1, var2) -> var1 * var2));
        System.out.println(machine.biOperation(2, 8, (var1, var2) -> var1 * var2));

    }
}

class Machine<T> {

    T  monoOperation(T var, MonoOperation<T> mono) {
        return mono.execute(var);
    }

    T biOperation(T var1, T var2, BiOperation<T> biOperation) {
        return biOperation.execute(var1, var2);
    }
}


