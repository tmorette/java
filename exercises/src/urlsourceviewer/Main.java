package urlsourceviewer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class Main {
    public static void main(String[] args) {
        GetUrlData.getData("http://www.google.pt");
    }

    private static class GetUrlData {
        private static void getData(String url) {

            BufferedReader data;

            try  {
                URL host = new URL(url);
                data = new BufferedReader(new InputStreamReader(host.openStream()));

                String line;

                while ((line = data.readLine()) != null) {
                    System.out.println(line);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
