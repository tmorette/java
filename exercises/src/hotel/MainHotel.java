package hotel;

public class MainHotel {
    public static void main(String[] args) {
        Hotel hotel = new Hotel(5);

        Guest g1 = new Guest("fernando");
        Guest g2 = new Guest("sara");
        Guest g3 = new Guest("filipe");

        g1.checkIn(hotel);
        g2.checkIn(hotel);
        g3.checkIn(hotel);
    }
}
