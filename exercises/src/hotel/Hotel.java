package hotel;

public class Hotel {
    private int numberOfRooms;
    private int[] roomNumbers;
    private Room[] rooms;

    public Hotel(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;

        this.roomNumbers = new int[numberOfRooms];
        this.rooms = new Room[numberOfRooms];
    }

    private Room checkRoom() {
        int number = generateRoomNumber();

        for (int i = 0; i < rooms.length; i++) {
            if (rooms[i] == null) {
                rooms[i] = new Room(number);
                rooms[i].changeVacancy();

                return rooms[i];
            }
        }

        return null;
    }


    public void verifyRoom(Guest guest) {
        Room guestRoom = checkRoom();

        if (guestRoom != null) {
            guest.setRoom(guestRoom);
            System.out.println("Hello " + guest.getName() + ", your room is number " + guest.getRoomNumber());
            return;
        }

        System.err.println("Sorry " + guest.getName() + " but there is no room available at the moment");
    }

    private int generateRoomNumber() {
        int number = (int) ((Math.random() * numberOfRooms) + 1);

        if (numberOfRooms == 1) {
            return number;
        }

        for (int room : roomNumbers) {
            if (number == room) {
                return generateRoomNumber();
            }
        }

        for (int i = 0; i < roomNumbers.length; i++) {
            if (roomNumbers[i] == 0) {
                roomNumbers[i] = number;
                break;
            }
        }

        return number;
    }
}
