package bidirectionalrange;

import java.util.Iterator;

public class Range {
    public static void main(String[] args) {

        BiDirectionalRange increaseIterator = new BiDirectionalRange(1, 5);
        for (int i : increaseIterator) {
            System.out.println(i);
        }
    }
}

class BiDirectionalRange implements Iterable<Integer> {

    private int first;
    private int second;

    BiDirectionalRange(int fist, int second) {
        this.first = fist;
        this.second = second;
    }

    @Override
    public Iterator<Integer> iterator() {
        if (second > first) {
            return new Iterator<>() {
                @Override
                public boolean hasNext() {
                    return second >= first;
                }

                @Override
                public Integer next() {
                    return first++;
                }
            };
        }

        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                return first >= second;
            }

            @Override
            public Integer next() {
                return first--;
            }
        };

    }
}