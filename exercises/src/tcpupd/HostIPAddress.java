package tcpupd;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class HostIPAddress {
    public static void main(String[] args) {
        try {

            InetAddress ip = InetAddress.getByName("www.google.com");
            System.out.println(ip);
            System.out.println(ip.getHostAddress());
            System.out.println(ip.isReachable(800));

        } catch (UnknownHostException e) {
            System.out.println("this host does not exist.");
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println("this host is not reacheable due to firewall protection.");
            System.out.println(e.getMessage());
        }
    }
}
