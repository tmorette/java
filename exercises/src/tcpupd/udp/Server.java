package tcpupd.udp;

import java.io.IOException;
import java.net.*;

public class Server {
    public static void main(String[] args) {
        new Server().init(args[0]);
    }

    private void init(String stringPort) {
        int port = Integer.parseInt(stringPort);
        byte[] pkgReceived = new byte[1024];

        try (DatagramSocket socket = new DatagramSocket(port)) {

            DatagramPacket pkgToReceive = new DatagramPacket(pkgReceived, pkgReceived.length);
            socket.receive(pkgToReceive);

            String data = new String(pkgToReceive.getData(), 0, pkgToReceive.getLength()).toUpperCase();

            DatagramPacket pkgToSend = new DatagramPacket(data.getBytes(), data.length(), pkgToReceive.getAddress(), pkgToReceive.getPort());
            socket.send(pkgToSend);


        } catch (SocketException e) {
            System.out.println("unable to create a new socket");
            System.out.println("Server#start(String, int)");
            System.out.println(e.getMessage());

        } catch (IOException e) {
            System.out.println("message could not be received");
            System.out.println("Server#start(String, int)");
            System.out.println(e.getMessage());
        }
    }
}
