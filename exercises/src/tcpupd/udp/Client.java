package tcpupd.udp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;

public class Client {
    public static void main(String[] args) {
        new Client().init();
    }

    private void init() {
        try (BufferedReader input = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.print("enter the address: ");
            String hostname = input.readLine();

            System.out.print("input the port number: ");
            String port = input.readLine();

            System.out.print("input a message: ");
            String message = input.readLine();

            start(hostname, Integer.parseInt(port), message);

        } catch (IOException e) {
            System.out.println("Client#init()");
            System.out.println(e.getMessage());
        }
    }

    private void start(String hostname, int port, String message) {

        byte[] sendMessage = message.getBytes();
        byte[] receivedMessage = new byte[1024];

        try (DatagramSocket socket = new DatagramSocket()){

            DatagramPacket pkgToSend = new DatagramPacket(sendMessage, sendMessage.length, InetAddress.getByName(hostname), port);
            socket.send(pkgToSend);

            DatagramPacket pkgToReceive = new DatagramPacket(receivedMessage, receivedMessage.length);
            socket.receive(pkgToReceive);

            System.out.println("Server: " + new String(pkgToReceive.getData(), 0, pkgToReceive.getLength()));

        } catch (SocketException e) {
            System.out.println("unable to create a new socket");
            System.out.println("Client#start(String, int)");
            System.out.println(e.getMessage());

        } catch (IOException e) {
            System.out.println("message could not be received");
            System.out.println("Client#start(String, int)");
            System.out.println(e.getMessage());

        }
    }
}
