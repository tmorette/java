package tcpupd.tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
    public static void main(String[] args) {
        new Client().init();
    }

    private void init() {

        try (BufferedReader input = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.print("inform the ip address: ");
            String hostname = input.readLine();

            System.out.print("inform the port number: ");
            String stringPortNumber = input.readLine();
            int port = Integer.parseInt(stringPortNumber);

            start(hostname, port, input);

        } catch (IOException e) {
            System.out.println("Client#init()");
            System.out.println(e.getMessage());
        }
    }

    private void start(String hostname, int port, BufferedReader input) {

        try (
                Socket clientSocket = new Socket(hostname, port);
                PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()))
        ) {

            if (clientSocket.isConnected()) {
                System.out.println("client is connected to port #" + clientSocket.getPort());
            }

            while (clientSocket.isConnected()) {
                System.out.print("message: ");
                String message = input.readLine();

                if (message.equals("quit")) {
                    out.println(message);
                    System.out.println("connection terminated");

                    clientSocket.close();
                    break;
                }

                out.println(message);

                if (in.readLine() == null) {
                    System.out.println("connection terminated by server");
                    break;
                }

                System.out.println("server: " + in.readLine());

            }

        } catch (IOException e) {
            System.out.println("Client#start(String, int)");
            System.out.println(e.getMessage());
        }
    }
}
