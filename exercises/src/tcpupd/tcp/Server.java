package tcpupd.tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) {
        new Server().init();
    }

    private void init() {

        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.print("inform your port to start server: ");
            String stringPort = input.readLine();
            int port = Integer.parseInt(stringPort);

            start(input, port);

        } catch (IOException e) {
            System.out.println("Server#init()");
            System.out.println(e.getMessage());
        }
    }

    private void start(BufferedReader input, int port) {
        try (ServerSocket serverSocket = new ServerSocket(port)) {

            while (!serverSocket.isClosed()) {
                Socket clientSocket = serverSocket.accept();
                PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

                if (serverSocket.isBound()) {
                    System.out.println("connection stabilished in port #" + serverSocket.getLocalPort());
                }

                String inputClient;

                while (clientSocket.isConnected()) {

                    if ((inputClient = in.readLine()).equals("quit")) {
                        System.out.println(
                                "connection terminated by client\n" +
                                        "waiting for another connection"
                        );

                        break;
                    }

                    System.out.println("client: " + inputClient);

                    System.out.print("server: ");
                    String message = input.readLine();

                    if (message.equals("quit")) {
                        System.out.println("connection terminated");
                        serverSocket.close();
                        break;
                    }

                    out.println(message);
                }
            }

        } catch (IOException e) {
            System.out.println("Server#init()");
            System.out.println(e.getMessage());
        }
    }
}
