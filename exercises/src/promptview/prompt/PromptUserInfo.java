package promptview.prompt;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.string.PasswordInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;
import promptview.users.User;
import promptview.users.UserList;

class PromptUserInfo {

    private UserList userList;
    private Prompt prompt;
    private String userName;
    private String userPassword;

    PromptUserInfo() {
        userList = new UserList();
        prompt = new Prompt(System.in, System.out);
    }

    void createUser() {

        if (userList.checkUserName(askForUserName())) {
            System.out.println("this name is already registered");
            PromptMenu.showMenu();
        }

        askForUserPassword();

        addUser(userName, userPassword);
        System.out.println("user created");

        PromptMenu.showMenu();
    }

    void login() {

        askForUserName();

        if (!userList.checkUserPassword(userName, askForUserPassword())) {
            System.out.println("password does not match");
            PromptMenu.showMenu();
        }

        System.out.println("user logged in");
        PromptMenu.showMenu();
    }

    void deleteUser() {
        if (!userList.checkUserName(askForUserName())) {
            System.out.println("name not found");
            PromptMenu.showMenu();
        }

        if (!userList.checkUserPassword(userName, askForUserPassword())) {
            System.out.println("password does not match");
            PromptMenu.showMenu();
        }

        delete(userName);
        System.out.println("user deleted");
        PromptMenu.showMenu();
    }

    void listUsers() {
        userList.listUserNames();
        PromptMenu.showMenu();
    }

    private String askForUserName() {

        StringInputScanner askName = new StringInputScanner();
        askName.setMessage("tell me your name: ");

        userName = prompt.getUserInput(askName);
        quit(userName);

        return userName;
    }

    private String askForUserPassword() {

        PasswordInputScanner askPassword = new PasswordInputScanner();
        askPassword.setMessage("tell me your password: ");

        userPassword = prompt.getUserInput(askPassword);
        quit(userPassword);

        return userPassword;
    }

    private void addUser(String name, String password) {
        userList.add(new User(name, password));
    }

    private void delete(String name) {
        userList.delete(name);
    }

    private void quit(String quit) {
        if (quit.equals("/quit")) {
            System.exit(0);
        }
    }

}
