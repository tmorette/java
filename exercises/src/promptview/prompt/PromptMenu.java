package promptview.prompt;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;

public abstract class PromptMenu {

    private static PromptUserInfo promptUserInfo = new PromptUserInfo();
    private static String[] menuOptions = {"Login", "Create User", "Delete User", "List Users", "Quit"};

    public static void showMenu() {
        Prompt prompt = new Prompt(System.in, System.out);

        MenuInputScanner menuInput = new MenuInputScanner(menuOptions);
        menuInput.setMessage("Welcome to the user screen\nType /quit to exit anytime");

        int userInput = prompt.getUserInput(menuInput);

        goToOptionChoosedByUser(userInput);
    }

    private static void goToOptionChoosedByUser(int userInput) {

        switch (userInput) {
            case 1:
                promptUserInfo.login();
            case 2:
                promptUserInfo.createUser();
            case 3:
                promptUserInfo.deleteUser();
            case 4:
                promptUserInfo.listUsers();
            default:
                System.exit(0);
        }
    }


}
