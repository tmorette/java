package promptview.users;

import java.util.ArrayList;
import java.util.List;

public class UserList {

    private List<User> userList;

    public UserList() {
        userList = new ArrayList<>();
    }

    public void add(User user) {
        userList.add(user);
    }

    public void delete(String name) {
        userList.removeIf(user -> user.getName().equals(name));
    }

    public void listUserNames() {

        if (userList.size() == 0) {
            System.out.println("any user found");
            return;
        }

        for (User user : userList) {
            System.out.println("user: " + user.getName());
        }
    }

    public boolean checkUserName(String userName) {

        for (User user : userList) {
            if (user.getName().equals(userName)){
                return true;
            }
        }

        return false;
    }

    public boolean checkUserPassword (String userName, String password) {

        for (User user : userList) {
            if (user.getName().equals(userName) && user.getPassword().equals(password)) {
                return true;
            }
        }

        return false;
    }
}
