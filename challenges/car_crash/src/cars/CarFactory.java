package cars;

public class CarFactory {

    public static Car getNewCar() {

        switch (CarType.random()) {
            case WOLKS:
                return new Wolks();
            case MUSTANG:
                return new Mustang();
            default:
                return new Fiat();
        }
    }
}
