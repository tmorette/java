package cars;

public enum CarType {
    MUSTANG('M', 3, 2),
    WOLKS('W', 2, 5),
    FIAT('F', 1, 6);

    private char symbol;
    private int speed;
    private int changeDirectionProbability;

    CarType(char symbol, int speed, int changeDirectionProbability) {
        this.symbol = symbol;
        this.speed = speed;
        this.changeDirectionProbability = changeDirectionProbability;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getSpeed() {
        return speed;
    }

    public int getChangeDirectionProbability() {
        return changeDirectionProbability;
    }

    public static CarType random () {
        int index = (int)(Math.random() * CarType.values().length);
        return CarType.values()[index];
    }
}
