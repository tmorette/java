package cars;

import com.googlecode.lanterna.terminal.Terminal;
import field.Position;

abstract public class Car {
    private Position pos;
    private int speed;
    private boolean crashed;

    private CarType car;
    private Terminal.Color backgroundColor;
    private Terminal.Color carColor;

    public Car(CarType car) {

        pos = new Position(car);
        speed = car.getSpeed();
        this.car = car;

        backgroundColor = Terminal.Color.WHITE;
        carColor = Terminal.Color.values()[(int) (Math.random() * Terminal.Color.values().length)];
    }

    public Terminal.Color getBackgroundColor() {
        return backgroundColor;
    }

    public Terminal.Color getCarColor() {
        return carColor;
    }

    public Position getPos() {
        return pos;
    }

    public boolean isCrashed() {
        return crashed;
    }

    public void crashed() {
        crashed = true;
    }

    public void move() {
        if (crashed) {
            return;
        }

        pos.move(this.speed);
    }

    @Override
    public String toString() {
        if (crashed) {
            return "C";
        }
        return Character.toString(car.getSymbol());
    }
}
