package field;

import cars.CarType;

public class Position {
    private int col;
    private int row;
    private Direction direction;
    private CarType car;

    public Position(CarType car) {
        this.col = (int) (Math.random() * Field.getWidth());
        this.row = (int) (Math.random() * Field.getHeight());

        this.car = car;

        direction = Direction.randomDirection();
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    private void changeDirection() {

        int random = (int) (Math.random() * 10);
        Direction newDirection = Direction.randomDirection();
        Direction oppositeDirection = Direction.oppositeDirection(direction);

        if (random < car.getChangeDirectionProbability() && newDirection != oppositeDirection) {
            direction = newDirection;
        }

        if (col < 1) {
            direction = Direction.RIGHT;
            return;
        }

        if (col > Field.getWidth() - 1) {
            direction = Direction.LEFT;
            return;
        }

        if (row < 1) {
            direction = Direction.DOWN;
            return;
        }

        if (row > Field.getHeight() - 1) {
            direction = Direction.UP;
        }
    }

    public void move(int speed) {
        changeDirection();

        switch (direction) {
            case UP:
                row -= speed;
                break;
            case DOWN:
                row += speed;
                break;
            case LEFT:
                col -= speed;
                break;
            case RIGHT:
                col += speed;
                break;
        }
    }

    public boolean comparePosition (Position position) {
        return row == position.getRow() && col == position.getRow();
    }
}
