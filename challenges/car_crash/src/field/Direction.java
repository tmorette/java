package field;

public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT;

    public static Direction randomDirection () {
        return Direction.values()[(int)(Math.random() * Direction.values().length)];
    }

    public static Direction oppositeDirection (Direction direction) {
        Direction opposite = Direction.UP;

        switch (direction) {
            case UP:
                opposite = Direction.DOWN;
                break;
            case DOWN:
                opposite = Direction.UP;
                break;
            case RIGHT:
                opposite = Direction.LEFT;
                break;
            case LEFT:
                opposite = Direction.RIGHT;
                break;
        }

        return opposite;
    }
}
