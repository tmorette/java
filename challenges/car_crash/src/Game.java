import cars.Car;
import cars.CarFactory;
import field.Field;

public class Game {

    public static final int MANUFACTURED_CARS = 20;

    /**
     * Container of Cars
     */
    private Car[] cars;

    /**
     * Animation delay
     */
    private int delay;

    public Game(int cols, int rows, int delay) {

        Field.init(cols, rows);
        this.delay = delay;

    }

    /**
     * Creates a bunch of cars and randomly puts them in the field
     */
    public void init() {

        cars = new Car[MANUFACTURED_CARS];
        for (int i = 0; i < cars.length; i++) {
            cars[i] = CarFactory.getNewCar();
        }

        Field.draw(cars);

    }

    /**
     * Starts the animation
     *
     * @throws InterruptedException
     */
    public void start() throws InterruptedException {

        while (true) {

            // Pause for a while
            Thread.sleep(delay);

            // Move all cars
            moveAllCars();

            // Update screen
            Field.draw(cars);

        }

    }

    private void fillPositions () {
        int[] row = new int[MANUFACTURED_CARS];
        int[] col = new int[MANUFACTURED_CARS];

        for (int i = 0; i < MANUFACTURED_CARS; i++) {
            row[i] = cars[i].getPos().getRow();
            col[i] = cars[i].getPos().getCol();
        }

        checkCrash(row, col);
    }

    private void checkCrash (int[] row, int[] col) {
        for (int i = 0; i < MANUFACTURED_CARS; i++) {
            for (int j = 0; j < MANUFACTURED_CARS; j++) {
                if (i == j) {
                    continue;
                }

                if (row[i] == row[j] && col[i] == col[j]) {
                    cars[i].crashed();
                    cars[j].crashed();
                }
            }
        }
    }

    private void moveAllCars() {
        fillPositions();

        for (Car car : cars) {
            car.move();

        }
    }
}
