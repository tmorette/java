package gfx.simplegfx;

import grid.Grid;
import grid.position.GridPosition;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;


public class SimpleGfxGrid implements Grid {

    private int rows;
    private int cols;
    private int x;
    private int y;
    private int cellSize;

    public static final int PADDING = 10;

    public SimpleGfxGrid(int cols, int rows){
        this.cols = cols;
        this.rows = rows;

        cellSize = PADDING;
        x = PADDING;
        y = PADDING;
    }

    /**
     * @see Grid#init()
     */
    @Override
    public void init() {
        Rectangle canvas = new Rectangle(PADDING, PADDING, getWidth(), getHeight());
        canvas.draw();
    }

    /**
     * @see Grid#getCols()
     */
    @Override
    public int getCols() {
        return cols;
    }

    /**
     * @see Grid#getRows()
     */
    @Override
    public int getRows() {
        return rows;
    }

    /**
     * Obtains the width of the grid in pixels
     * @return the width of the grid
     */
    public int getWidth() {
        return cols * getCellSize();
    }

    /**
     * Obtains the height of the grid in pixels
     * @return the height of the grid
     */
    public int getHeight() {
        return rows * getCellSize();
    }

    /**
     * Obtains the grid X position in the SimpleGFX canvas
     * @return the x position of the grid
     */
    public int getX() {
        return x;
    }

    /**
     * Obtains the grid Y position in the SimpleGFX canvas
     * @return the y position of the grid
     */
    public int getY() {
        return y;
    }

    /**
     * Obtains the pixel width and height of a grid position
     * @return cellSize
     */
    public int getCellSize() {
        return cellSize;
    }

    /**
     * @see Grid#makeGridPosition()
     */
    @Override
    public GridPosition makeGridPosition() {
        return new SimpleGfxGridPosition(this);
    }

    /**
     * @see Grid#makeGridPosition(int, int)
     */
    @Override
    public GridPosition makeGridPosition(int col, int row) {
        return new SimpleGfxGridPosition(col, row, this);
    }

    /**
     * Auxiliary method to compute the y value that corresponds to a specific row
     * @param row index
     * @return y pixel value
     */
    public int rowToY(int row) {
        return (row * getCellSize()) + PADDING;
    }

    /**
     * Auxiliary method to compute the x value that corresponds to a specific column
     * @param column index
     * @return x pixel value
     */
    public int columnToX(int column) {
        return (column * getCellSize()) + PADDING;
    }
}
