package gfx.simplegfx;

import grid.GridColor;
import grid.GridDirection;
import grid.position.AbstractGridPosition;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

/**
 * Simple graphics position
 */
public class SimpleGfxGridPosition extends AbstractGridPosition {

    private Rectangle rectangle;
    private SimpleGfxGrid simpleGfxGrid;

    /**
     * Simple graphics position constructor
     * @param grid Simple graphics grid
     */
    public SimpleGfxGridPosition(SimpleGfxGrid grid){
        super((int) (Math.random() * grid.getCols()), (int) (Math.random() * grid.getRows()), grid);

        rectangle = new Rectangle(grid.columnToX(super.getCol()), grid.rowToY(super.getRow()), grid.getCellSize(), grid.getCellSize());

        simpleGfxGrid = grid;
        show();
    }

    /**
     * Simple graphics position constructor
     * @param col position column
     * @param row position row
     * @param grid Simple graphics grid
     */
    public SimpleGfxGridPosition(int col, int row, SimpleGfxGrid grid){
        super(col, row, grid);

        rectangle = new Rectangle(grid.columnToX(super.getCol()), grid.rowToY(super.getRow()), grid.getCellSize(), grid.getCellSize());

        simpleGfxGrid = grid;
        show();
    }

    /**
     * @see GridPosition#show()
     */
    @Override
    public void show() {
        rectangle.fill();
    }

    /**
     * @see GridPosition#hide()
     */
    @Override
    public void hide() {
        rectangle.delete();
    }

    /**
     * @see GridPosition#moveInDirection(GridDirection, int)
     */
    @Override
    public void moveInDirection(GridDirection direction, int distance) {

        int row = super.getRow();
        int col = super.getCol();

        super.moveInDirection(direction, distance);

        rectangle.translate(
                (super.getCol() - col) * simpleGfxGrid.getCellSize(),
                (super.getRow() - row) * simpleGfxGrid.getCellSize()

        );

    }

    /**
     * @see AbstractGridPosition#setColor(GridColor)
     */
    @Override
    public void setColor(GridColor color) {
        rectangle.setColor(SimpleGfxColorMapper.getColor(color));
        super.setColor(color);
    }
}
