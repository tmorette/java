package main;

import main.cell.Cell;
import main.file.FileHandler;
import main.input.InputKeyboard;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import java.util.List;
import java.util.ArrayList;

public class Grid {
    private static final int PADDING = 10;
    private static final int CELL_SIZE = 20;

    private Cursor cursor;

    private List<Cell> cells = new ArrayList<>();

    public Grid(int size, String file) {

        new Rectangle(PADDING, PADDING, size * CELL_SIZE, size * CELL_SIZE).draw();

        makeGrid(size);
        cursor = new Cursor();
        new InputKeyboard().init(this, new FileHandler(file));
    }

    private void makeGrid(int size) {
        int sideSize = (int) Math.pow(size, 2);

        for (int i = 0; i < sideSize; i++) {
            int col = i / size;
            int row = i % size;

            Cell cell = new Cell(col, row);
            cells.add(cell);
        }

    }

    private boolean checkLimits(Cursor.Direction direction) {
        int size = (int) Math.sqrt(cells.size());

        switch (direction) {
            case UP:
                return cursor.getRow() > 0;
            case DOWN:
                return cursor.getRow() < size - 1;
            case LEFT:
                return cursor.getCol() > 0;
            case RIGHT:
                return cursor.getCol() < size - 1;
        }

        throw new IllegalArgumentException("main.Grid#checkLimits(main.Cursor.Direction");
    }

    public static int getPADDING() {
        return PADDING;
    }

    public static int getCellSize() {
        return CELL_SIZE;
    }

    public void move(Cursor.Direction direction) {
        if (!checkLimits(direction)) {
            return;
        }

        cursor.move(direction);
    }

    public void paint() {
        for(Cell cell : cells) {
            if (cell.equals(cursor)) {
                cell.togglePaint();
                return;
            }
        }
    }
}
