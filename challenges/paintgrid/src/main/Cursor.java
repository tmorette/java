package main;


import main.cell.AbstractCell;

public class Cursor extends AbstractCell {
    public Cursor() {
        super(0, 0);
        rectangle.fill();
    }

    public void move(Direction direction) {
        int colDifferential = direction.col;
        int rowDifferential = direction.row;

        col += colDifferential;
        row += rowDifferential;

        super.rectangle.translate(colDifferential * Grid.getCellSize(), rowDifferential * Grid.getCellSize());
    }

    public enum Direction {
        UP(0, -1),
        DOWN(0, 1),
        LEFT(-1, 0),
        RIGHT(1, 0);

        private int col;
        private int row;

        Direction(int col, int row) {
            this.col = col;
            this.row = row;
        }
    }
}
