package main.file;

import java.io.*;

public class FileHandler {
    private String file;

    public FileHandler(String file) {
        this.file = file;
    }

    public void save(String text) {
        try (PrintWriter writer = new PrintWriter(new FileWriter(file), true)) {
            writer.println(text);
        } catch (IOException e) {
            System.out.println("FileHandler#save(String)");
            System.out.println(e.getMessage());
        }
    }

    public String load() {
        String text = "";

        try (BufferedReader loader = new BufferedReader(new FileReader(file))) {
            text = loader.readLine();
        } catch (IOException e) {
            System.out.println("FileHandler#load()");
            System.out.println(e.getMessage());
        }

        return text;
    }
}
