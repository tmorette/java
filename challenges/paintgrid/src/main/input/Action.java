package main.input;

public interface Action {
    void execute();
}
