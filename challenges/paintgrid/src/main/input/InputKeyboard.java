package main.input;

import main.Cursor;
import main.Grid;
import main.file.FileHandler;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

import java.util.HashMap;
import java.util.Map;

public class InputKeyboard implements KeyboardHandler {

    private Map<Integer, Action> actions = new HashMap<>();

    public void init(Grid grid, FileHandler fileHandler) {
        mapKeys(grid, fileHandler);

        Keyboard keyboard = new Keyboard(this);

        for (Integer key : actions.keySet()) {
            KeyboardEvent event = new KeyboardEvent();
            event.setKey(key);
            event.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
            keyboard.addEventListener(event);
        }
    }

    public void mapKeys(Grid grid, FileHandler fileHandler) {
        actions.put(KeyboardEvent.KEY_UP, () -> grid.move(Cursor.Direction.UP));
        actions.put(KeyboardEvent.KEY_DOWN, () -> grid.move(Cursor.Direction.DOWN));
        actions.put(KeyboardEvent.KEY_LEFT, () -> grid.move(Cursor.Direction.LEFT));
        actions.put(KeyboardEvent.KEY_RIGHT, () -> grid.move(Cursor.Direction.RIGHT));
        actions.put(KeyboardEvent.KEY_SPACE, grid::paint);
        actions.put(KeyboardEvent.KEY_S, () -> fileHandler.save(grid.toString()));
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        Action action = actions.get(keyboardEvent.getKey());
        action.execute();
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
}
