package main.cell;

public class Cell extends AbstractCell {

    private boolean painted;

    public Cell(int col, int row) {
        super(col, row);
        rectangle.draw();

        painted = false;
    }

    public void togglePaint() {
        if (painted) {
            clear();
            return;
        }

        paint();
    }

    private void paint() {
        painted = true;
        rectangle.fill();
    }

    private void clear() {
        painted = false;
        rectangle.draw();
    }
}
