package main.cell;

import main.Grid;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public abstract class AbstractCell {
    protected int col;
    protected int row;

    protected Rectangle rectangle;

    public AbstractCell(int col, int row) {
        this.col = col;
        this.row = row;

        rectangle = new Rectangle(pixel(col), pixel(row), Grid.getCellSize(), Grid.getCellSize());
    }

    private int pixel (int value) {
        return value * Grid.getCellSize() + Grid.getPADDING();
    }

    public void show() {
        rectangle.draw();
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof AbstractCell)) {
            return false;
        }

        AbstractCell cell = (AbstractCell) object;
        return col == cell.col && row == cell.row;
    }
}
