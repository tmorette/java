package client;

import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ClientConnection implements Runnable {

    private Socket socket;
    private static final int THREAD_POOL_SIZE = 10;


    public ClientConnection(Socket socket) {
        this.socket = socket;
        run();
    }

    @Override
    public void run() {
        ExecutorService poolThread = Executors.newFixedThreadPool(THREAD_POOL_SIZE);

        for (int i = 0; i < 20; i++) {
            poolThread.execute(new Thread(new Client(socket)));
        }
    }
}
