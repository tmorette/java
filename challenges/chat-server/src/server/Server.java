package server;


import client.ClientConnection;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    public static final int PORT = 8080;

    public void start() {

        try {
            ServerSocket serverSocket = new ServerSocket(PORT);

            while (!serverSocket.isClosed()) {
                new ClientConnection(serverSocket.accept());
            }

        } catch (IOException e) {
            System.out.println("Server#start()");
            System.out.println(e.getMessage());
        }

    }
}
