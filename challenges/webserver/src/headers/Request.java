package headers;

public class Request {

    public String getRequestPath(String request) {
        System.out.println("request\n" + request);

        String[] responseArray = request.split(" ");

        if (responseArray.length > 1 && responseArray[1].equals("/")) {
            return "/index.html";
        }

        return responseArray[1];
    }
}
