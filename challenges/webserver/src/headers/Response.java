package headers;

import FileHandler.FileHandler;

public class Response {

    public String sendResponse(String path) {

        FileHandler fileHandler = new FileHandler();

        if (path.equals("/") || path.equals("/index.html")) {

            return "HTTP/1.0 200 Document Follows\r\n" +
                    "Content-Type: text/html; charset=UTF-8\r\n" +
                    "Content-Length: " + fileHandler.getFileSize("resources/index.html") + "\r\n" +
                    "\r\n";
        }

        if (path.equals("/academia.jpg")) {

            return "HTTP/1.0 200 Document Follows\r\n" +
                    "Content-Type: image/jpg\r\n" +
                    "Content-Length: " + fileHandler.getFileSize("resources/academia.jpg") + "\r\n" +
                    "\r\n";

        }

        return null;

    }
}
