package FileHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class FileHandler {

    private int size;
    private FileInputStream fileInputStream;

    public int getFileSize(String path) {

        try {

            File file = new File(path);
            fileInputStream = new FileInputStream(file);

            size = Math.toIntExact(fileInputStream.getChannel().size());

        } catch (IOException e) {
            System.out.println("FileHandler#getFileSize(String)");
            System.out.println(e.getMessage());
        }

        return size;
    }

    public FileInputStream getFile(String path) {

        try {

            File file = new File("resources" + path);
            fileInputStream = new FileInputStream(file);

        } catch (IOException e) {
            System.out.println("FileHandler#getFileSize(String)");
            System.out.println(e.getMessage());
        }

        return fileInputStream;
    }

}
