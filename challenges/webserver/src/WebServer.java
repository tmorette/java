import FileHandler.FileHandler;
import headers.Request;
import headers.Response;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

class WebServer {

    private static final int PORT = 8080;
    private FileHandler fileHandler;
    private Request request;
    private Response response;

    WebServer() {
        fileHandler = new FileHandler();
        request = new Request();
        response = new Response();
    }

    void connect() {

        try (
                ServerSocket server = new ServerSocket(PORT)
        ) {

            while (!server.isClosed()) {
                Socket clientSocket = server.accept();

                BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                DataOutputStream output = new DataOutputStream(clientSocket.getOutputStream());

                String clientRequest = request.getRequestPath(reader.readLine());

                String header = response.sendResponse(clientRequest);
                System.out.println("header\n" + header);

                output.writeBytes(header);
                output.write(fileHandler.getFile(clientRequest).readAllBytes());
                output.flush();

                clientSocket.close();
            }

        } catch (IOException e) {
            System.out.println("WebServer#connect(int)");
            System.out.println(e.getMessage());
        }
    }
}
