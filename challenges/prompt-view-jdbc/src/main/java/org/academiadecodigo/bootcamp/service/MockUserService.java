package org.academiadecodigo.bootcamp.service;

import org.academiadecodigo.bootcamp.model.User;
import org.academiadecodigo.bootcamp.persistence.Connect;
import org.academiadecodigo.bootcamp.utils.Security;

import java.sql.*;
import java.util.*;

/**
 * An in-memory mock user service backed by a HashMap
 */
public class MockUserService implements UserService {

    private Connection connect;

    public MockUserService() {
        connect = new Connect().getConnection();
    }

    /**
     * @see UserService#authenticate(String, String)
     */
    @Override
    public boolean authenticate(String username, String password) {
        String usernameValue = "";
        String passwordValue = "";

        String query = "SELECT password FROM users WHERE username = ?;";

        try (PreparedStatement statement = connect.prepareStatement(query)) {

            statement.setString(1, username);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                passwordValue = resultSet.getString("password");
            }

        } catch (SQLException e) {
            System.out.println("MockUserService#authenticate(String, String)");
            System.out.println(e.getMessage());
        }

        return Security.getHash(password).equals(passwordValue);
    }

    /**
     * @see UserService#add(User)
     */
    @Override
    public void add(User user) {

        if (findByName(user.getUsername()) != null) {
            return;
        }

        String query = "INSERT INTO users(username, password, email, firstName, lastName, phone) " +
                "VALUES (?, ?, ?, ?, ?, ?);";

        try (PreparedStatement statement = connect.prepareStatement(query)) {
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getLastName());
            statement.setString(6, user.getPhone());

            statement.executeUpdate();

        } catch (SQLException e) {
            System.out.println("MockUserService#add(User)");
            System.out.println(e.getMessage());
        }
    }

    /**
     * @see UserService#findByName(String)
     */
    @Override
    public User findByName(String username) {

        User user = null;
        String query = "SELECT username, firstName, lastName, phone, email, password FROM users WHERE username = ?;";

        try (PreparedStatement statement = connect.prepareStatement(query)) {

            statement.setString(1, username);
            ResultSet resultSet = statement.executeQuery();

            if(resultSet.next()) {

                user = new User(
                        resultSet.getString("username"),
                        resultSet.getString("email"),
                        resultSet.getString("password"),
                        resultSet.getString("firstName"),
                        resultSet.getString("lastName"),
                        resultSet.getString("phone")
                        );
            }

        } catch (SQLException e) {
            System.out.println("MockUserService#findByName(String)");
            System.out.println(e.getMessage());
        }

        return user;
    }


    /**
     * @see UserService#findAll()
     */
    @Override
    public List<User> findAll() {
        List<User> userList = new ArrayList<>();
        User user = null;

        String query = "SELECT * FROM users";

        try (Statement statement = connect.createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                user = new User(
                        resultSet.getString("username"),
                        resultSet.getString("email"),
                        resultSet.getString("password"),
                        resultSet.getString("firstName"),
                        resultSet.getString("lastName"),
                        resultSet.getString("phone")
                );

                userList.add(user);
            }

        } catch (SQLException e) {
            System.out.println("MockUserService#findAll()");
            System.out.println(e.getMessage());
        }

        return userList;
    }


    /**
     * @see UserService#count()
     */
    @Override
    public int count() {

        String userQuery = "SELECT COUNT(*) FROM users";
        int result = 0;

        try (Statement statement = connect.createStatement()) {
            ResultSet resultSet = statement.executeQuery(userQuery);

            if (resultSet.next()) {
                result = resultSet.getInt(1);
            }

        } catch (SQLException e) {
            System.out.println("MockUserService#count()");
            System.out.println(e.getMessage());
        }

        return result;
    }


}
