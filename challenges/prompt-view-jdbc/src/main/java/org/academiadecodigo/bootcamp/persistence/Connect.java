package org.academiadecodigo.bootcamp.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect implements OpenConnection {

    private Connection connection;

    @Override
    public Connection getConnection() {

        String url = "jdbc:mysql://localhost:3306/user_database?user=thiago";

        try {
            if (connection == null) {
                connection = DriverManager.getConnection(url);
            }
        } catch (SQLException e) {
            System.out.println("Connect#getConnection()");
            System.out.println(e.getMessage());
        }

        return connection;
    }

    public void close() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            System.out.println("Connect#close()");
            System.out.println(e.getMessage());
        }
    }
 }
