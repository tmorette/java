package org.academiadecodigo.bootcamp.persistence;


import java.sql.Connection;

public interface OpenConnection {

    public Connection getConnection();

}
