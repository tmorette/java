DROP DATABASE IF EXISTS user_database;

CREATE DATABASE user_database;
USE user_database;

CREATE TABLE users(
    id INTEGER AUTO_INCREMENT,
    username VARCHAR(64) UNIQUE,
    password VARCHAR(64),
    email VARCHAR(64),
    firstName VARCHAR(64),
    lastName VARCHAR(64),
    phone CHAR(64),
    PRIMARY KEY(id)
);