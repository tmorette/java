package balloon;

import grid.GridPosition;

public class OrangeBalloon extends Balloon {

    public OrangeBalloon(GridPosition gridPosition, int colStartPosition, int rowStartPosition) {
        super(gridPosition, BalloonType.ORANGE, BalloonType.ORANGE.getPicture(), colStartPosition, rowStartPosition);
    }
}
