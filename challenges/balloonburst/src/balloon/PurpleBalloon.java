package balloon;

import grid.GridPosition;

public class PurpleBalloon extends Balloon {

    public PurpleBalloon(GridPosition gridPosition, int colStartPosition, int rowStartPosition) {
        super(gridPosition, BalloonType.PURPLE, BalloonType.PURPLE.getPicture(), colStartPosition, rowStartPosition);
    }
}
